import os
import sys
import re

from PIL import Image, ImageEnhance
from keras.preprocessing.image import Iterator
from sklearn.metrics import auc
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve, roc_auc_score, precision_recall_curve
from scipy.misc import imresize
import pandas as pd

import random
import matplotlib.pyplot as plt
import numpy as np
import pickle
import matplotlib
from skimage import measure


def outputs2labels(outputs, min_val, max_val):
    return np.clip(np.round(outputs), min_val, max_val)


def all_files_under(path, extension=None, header=None , append_path=True, sort=True):
    if append_path:
        if extension is None:
            if header is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.startswith(header)]
        else:
            if header is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.startswith(header) and fname.endswith(extension)]
    else:
        if extension is None:
            if header is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.startswith(header)]
        else:
            if header is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.startswith(header) and fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def imagefiles2arrs(filenames):
    img_shape = image_shape(filenames[0])
    images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)

    for file_index in xrange(len(filenames)):
        im = Image.open(filenames[file_index])
        img = np.array(im)
        images_arr[file_index] = img

    return images_arr


def crop(imgs):
    assert len(imgs.shape) == 4
    for index in range(imgs.shape[0]):
        img = imgs[index]
    
        # set params
        h_ori, w_ori, _ = np.shape(img)
        red_threshold = 20
        roi_check_len = h_ori // 5
        
        # Find Connected Components with intensity above the threshold
        blobs_labels, n_blobs = measure.label(img[:, :, 0] > red_threshold, return_num=True)
        if n_blobs == 0:
            print ("crop failed [no blob found]")
            return 0, 0, 0, 0
       
        # Find the Index of Connected Components of the Fundus Area (the central area)
        majority_vote = np.argmax(np.bincount(blobs_labels[h_ori // 2 - roi_check_len // 2:h_ori // 2 + roi_check_len // 2,
                                                                w_ori // 2 - roi_check_len // 2:w_ori // 2 + roi_check_len // 2].flatten()))
        if majority_vote == 0:
            print ("crop failed [invisible areas (intensity in red channel less than " + str(red_threshold) + ") are dominant in the center]")
            return 0, 0, 0, 0
        
        row_inds, col_inds = np.where(blobs_labels == majority_vote)
        row_max = np.max(row_inds)
        row_min = np.min(row_inds)
        col_max = np.max(col_inds)
        col_min = np.min(col_inds)
        if row_max - row_min < 100 or col_max - col_min < 100:
            print n_blobs
            for i in range(1, n_blobs + 1):
                print len(blobs_labels[blobs_labels == i])
            print ("crop failed for [too small ocular areas]")
            return 0, 0, 0, 0 
        
        # crop the image 
        crop_img = img[row_min:row_max, col_min:col_max]
        max_len = max(crop_img.shape[0], crop_img.shape[1])
        img_h, img_w, _ = crop_img.shape
        padded = np.zeros((max_len, max_len, 3), dtype=np.uint8)
        padded[(max_len - img_h) // 2:(max_len - img_h) // 2 + img_h, (max_len - img_w) // 2:(max_len - img_w) // 2 + img_w, ...] = crop_img
 
    cropped_w = max_len
    y_offset = row_min - (max_len - img_h) // 2
    x_offset = col_min - (max_len - img_w) // 2
    return np.expand_dims(padded, axis=0), y_offset, x_offset, cropped_w 


def resize_imgs(imgs, target_size):
    h_target, w_target = target_size
    assert len(imgs.shape) == 4
    resized_imgs = np.zeros((imgs.shape[0], h_target, w_target, imgs.shape[3]))
    for index in range(resized_imgs.shape[0]):
        img = imgs[index]
        resized_imgs[index, ...] = imresize(img, (h_target, w_target), 'bicubic')

    return resized_imgs 


def preprocess(filenames):
    ori_images_arr = imagefiles2arrs(filenames)

    # crop around center and resize
    cropped_imgs, y_offset, x_offset, cropped_w = crop(ori_images_arr)
    images_arr = resize_imgs(cropped_imgs, (512, 512))

    # convert to z score per image
    for file_index in xrange(len(filenames)):
        img = images_arr[file_index]
        means = np.zeros(3)
        stds = np.zeros(3)
        for i in range(3):
            means[i] = np.mean(img[..., i][img[..., i] > 5])
            stds[i] = np.std(img[..., i][img[..., i] > 5])
        images_arr[file_index] = (img - means) / (stds + 0.1)

    return images_arr
    
