# Fundus Laterality #

###  Laterality Classification of Fundus Images using Interpretable Deep Neural Networks [[original paper]()] [[cite]()]###

## Package Dependency ##
matplotlib==1.3.1  
numpy==1.14.3  
scikit_image==0.10.1  
pandas==0.21.1   
scipy==0.19.1  
Keras==2.1.6  
Pillow==5.1.0   
skimage==0.0  
scikit_learn==0.19.1  

## Running the code ##
At the home directory, install required packages with

``` pip install requirements.txt```

Run inferece with 

``` python inference.py --test_dir=<folder_path> --gpu_index=<gpu_index>```

For instance, run inferece for the sample dataset ([IDRiD dataset](https://idrid.grand-challenge.org/)) with 

``` python inference.py --test_dir=IDRiD_imgs/ --gpu_index=0```

IDRiD_NAR_013.jpg is wrongly classified in the sample dataset.

## File Hierarchy ##
.
├── IDRiD_imgs  
├── inference.py  
├── model  
│   └── relu_softmax_weight_epoch49.h5  
├── network.py  
├── README.md  
├── requirements.txt  
├── results.csv  
└── utils.py  

IDRiD_imgs: sample fundus images
inference.py: inference file to run
model/relu_softmax_weight_epoch49.h5: weight file
network.py: network file in keras style
results.csv: results of laterality with columns of filename and the results (R or L)
utils.py: miscellaneous helper functions

## LICENSE ##
This is under the MIT License  
Copyright (c) 2018 Vuno Inc. (www.vuno.co)  
