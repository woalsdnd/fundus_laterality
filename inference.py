import utils
import os
import argparse
import time
import pandas as pd
from network import network_lat
import numpy as np

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--test_dir',
    type=str,
    help="Directory of validation files",
    required=True
    )
parser.add_argument(
    '--gpu_index',
    type=str,
    help="GPU index",
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set gpu 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# load a network
network = network_lat((512, 512, 3))
weight_file = utils.all_files_under("./model", extension=".h5")
assert len(weight_file) == 1
network.load_weights(weight_file[0])

# run test
start_time = time.time()
pred_labels = []
filenames = utils.all_files_under(FLAGS.test_dir)
for filename in filenames:
    input_img = utils.preprocess([filename])
    pred_labels += np.argmax(network.predict(input_img), axis=1).tolist()
print "duration : {}".format(time.time() - start_time)

# output results
decisions = []
labels = utils.outputs2labels(pred_labels, 0, 1)
for label in labels:
    decisions.append('R' if label == 0 else 'L')
df = pd.DataFrame({'filename':[os.path.basename(filename) for filename in filenames], 'laterality':decisions})
df.to_csv("results.csv", index=False)
