from keras.layers import Input, Conv2D, MaxPooling2D, Flatten, Dense, Dropout
from keras import regularizers
from keras.optimizers import SGD
from keras.models import Model


def network_lat(img_shape):
    img_h, img_w, img_ch = img_shape[0], img_shape[1], img_shape[2]
    n_filters = 32
    drop_rate = 0.5
    l2_coeff = 0.0005

    conv = {'kernel_size': (3, 3),
            'padding': 'same',
            'activation': 'relu',
            'kernel_regularizer': regularizers.l2(l2_coeff),
            'bias_regularizer': regularizers.l2(l2_coeff)}
    dense = {'kernel_regularizer': regularizers.l2(l2_coeff),
             'bias_regularizer': regularizers.l2(l2_coeff)}

    inputs = Input((img_h, img_w, img_ch), name='input_layer')
    conv1 = Conv2D(n_filters, strides=(2, 2), name='block1_conv1', **conv)(inputs)
    conv1 = Conv2D(n_filters, name='block1_conv2', **conv)(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2), name='block1_pool')(conv1)

    conv2 = Conv2D(2 * n_filters, strides=(2, 2), name='block2_conv1', **conv)(pool1)
    conv2 = Conv2D(2 * n_filters, name='block2_conv2', **conv)(conv2)
    conv2 = Conv2D(2 * n_filters, name='block2_conv3', **conv)(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2), name='block2_pool')(conv2)

    conv3 = Conv2D(4 * n_filters, name='block3_conv1', **conv)(pool2)
    conv3 = Conv2D(4 * n_filters, name='block3_conv2', **conv)(conv3)
    conv3 = Conv2D(4 * n_filters, name='block3_conv3', **conv)(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2), name='block3_pool')(conv3)

    conv4 = Conv2D(8 * n_filters, name='block4_conv1', **conv)(pool3)
    conv4 = Conv2D(8 * n_filters, name='block4_conv2', **conv)(conv4)
    conv4 = Conv2D(8 * n_filters, name='block4_conv3', **conv)(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2), name='block4_pool')(conv4)

    conv5 = Conv2D(16 * n_filters, name='block5_conv1', **conv)(pool4)
    conv5 = Conv2D(16 * n_filters, name='block5_conv2', **conv)(conv5)

    flatten = Flatten()(conv5)
    drop1 = Dropout(drop_rate)(flatten)
    dense1 = Dense(256, activation='relu', name='fc1', **dense)(drop1)

    drop2 = Dropout(drop_rate)(dense1)
    outputs = Dense(2, activation='softmax', name='fc2', **dense)(drop2)

    model = Model(inputs, outputs)
    model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss='categorical_crossentropy', metrics=['accuracy'])

    return model